package com.wr15.mytestproject.server;

public class ServerMovieHost {
    public static String BASEURL = "http://api.themoviedb.org/3/";
    public static String APIKEY = "api_key=859e1e2595ca61e03a724fb8889e0ddb";
    public static String DETAIL_FILM = "http://api.themoviedb.org/3/movie/";
    public static String BAHASA = "&language=en-US";
    public static String MOVIE_PLAYNOW = "movie/now_playing?";
    public static String MOVIE_POPULAR = "discover/movie?";
    public static String URLIMAGE = "https://image.tmdb.org/t/p/w780/";
    public static String MOVIE_VIDEO = "movie/";
    public static String GENRE_ANIMASI = "&with_genres=16";
    public static String GENRE_ACTION = "&with_genres=28";
    public static String GENRE_ADVENTURE = "&with_genres=12";
    public static String GENRE_KOMEDI = "&with_genres=35";
    public static String GENRE_HORROR = "&with_genres=27";



}
